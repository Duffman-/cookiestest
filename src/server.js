var express = require('express');
var bodyParser = require('body-parser');

var app = express();
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", true);
    next();
});

require('./config/express')(app);
require('./routes/index')(app);

module.exports = app;