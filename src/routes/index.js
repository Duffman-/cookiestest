module.exports = function (router) {
    router.get('/', function(req,res) {
        res.sendFile('view/index.html', {root: "src/"});
    });

    router.get('/getCookie', function(req,res) {
        var test = "test-string"
        res.cookie('test-cookie', test, {
            httpOnly: true,
            sameSite: true,
            signed: false,
            secure: true,
            expires: new Date(Date.now() + 900000000)
        });
        return res.json({});
    });
}
